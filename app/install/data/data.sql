SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for {{$pk}}admin_admin
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_admin`;
CREATE TABLE `{{$pk}}admin_admin` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `username` varchar(30) NOT NULL COMMENT '用户名，登陆使用',
    `password` varchar(30) NOT NULL COMMENT '用户密码',
    `nickname` varchar(30) NOT NULL COMMENT '用户昵称',
    `leader_id` int(11) DEFAULT NULL COMMENT '上级id',
    `profession_id` int(11) DEFAULT NULL COMMENT '职位id',
    `dept_id` int(11) DEFAULT '0' COMMENT '部门id',
    `dept_name` varchar(155) DEFAULT NULL COMMENT '部门名称',
    `sex` tinyint(2) DEFAULT '1' COMMENT '1 男  2 女',
    `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户状态：1正常,2禁用 默认1',
    `token` varchar(60) DEFAULT NULL COMMENT 'token',
    `job_number` varchar(155) DEFAULT NULL COMMENT '工号',
    `induction_date` date DEFAULT NULL COMMENT '入职日期',
    `year_holidays` int(5) DEFAULT NULL COMMENT '年假天数',
    `probation` date DEFAULT NULL COMMENT '试用期截止日期',
    `is_formal` tinyint(2) DEFAULT '1' COMMENT '1: 转正 2：未转正',
    `job_status` tinyint(2) DEFAULT '1' COMMENT '1:在职 2:离职',
    `quit_date` date DEFAULT NULL COMMENT '离职日期',
    `quit_reason` varchar(255) DEFAULT NULL COMMENT '离职原因',
    `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
    `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
    `delete_time` timestamp NULL DEFAULT NULL COMMENT '删除时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='管理表';

-- ----------------------------
-- Records of {{$pk}}admin_admin
-- ----------------------------
BEGIN;
INSERT INTO `{{$pk}}admin_admin` VALUES (1, 'admin', 'adc3949ba59abbe56e057f20f8', '超级管理员', NULL, NULL, 0, NULL, 1, 1, 'I639J9VAUJ9Ih3WizGnEmbiRflzzWecW11637491687.3668', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2021-11-21 18:48:07', NULL);
COMMIT;

-- ----------------------------
-- Table structure for {{$pk}}admin_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_admin_log`;
CREATE TABLE `{{$pk}}admin_admin_log` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `uid` int(11) DEFAULT NULL COMMENT '管理员ID',
    `url` varchar(255) NOT NULL DEFAULT '' COMMENT '操作页面',
    `desc` text COMMENT '日志内容',
    `ip` varchar(20) NOT NULL DEFAULT '' COMMENT '操作IP',
    `user_agent` text NOT NULL COMMENT 'User-Agent',
    `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员日志';

-- ----------------------------
-- Records of {{$pk}}admin_admin_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for {{$pk}}admin_admin_permission
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_admin_permission`;
CREATE TABLE `{{$pk}}admin_admin_permission` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `admin_id` int(11) DEFAULT NULL COMMENT '用户ID',
    `permission_id` int(11) DEFAULT NULL COMMENT '权限ID',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理-权限中间表';

-- ----------------------------
-- Records of {{$pk}}admin_admin_permission
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for {{$pk}}admin_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_admin_role`;
CREATE TABLE `{{$pk}}admin_admin_role` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `admin_id` int(11) DEFAULT NULL COMMENT '用户ID',
    `role_id` int(11) DEFAULT NULL COMMENT '角色ID',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='管理-角色中间表';

-- ----------------------------
-- Records of {{$pk}}admin_admin_role
-- ----------------------------
BEGIN;
INSERT INTO `{{$pk}}admin_admin_role` VALUES (1, 2, 2);
COMMIT;

-- ----------------------------
-- Table structure for {{$pk}}admin_article
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_article`;
CREATE TABLE `{{$pk}}admin_article` (
    `article_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文章id',
    `cate_id` int(11) DEFAULT NULL COMMENT '所属分类',
    `title` varchar(55) DEFAULT NULL COMMENT '标题',
    `desc` varchar(255) DEFAULT NULL COMMENT '描述',
    `author` varchar(55) DEFAULT NULL COMMENT '作者',
    `pub_time` date DEFAULT NULL COMMENT '发布日期',
    `content` text COMMENT '文章内容',
    `views` int(11) DEFAULT '0' COMMENT '阅读量',
    `is_delete` tinyint(2) DEFAULT '1' COMMENT '1 正常 2 删除',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Table structure for {{$pk}}admin_car
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_car`;
CREATE TABLE `{{$pk}}admin_car` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '车辆id',
    `car_type` tinyint(2) DEFAULT NULL COMMENT '车辆类型 1:轿车 2:suv 3:mpv 4:皮卡 5:货车',
    `passengers` int(3) DEFAULT NULL COMMENT '可乘坐人数',
    `band` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '车辆品牌',
    `car_number` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '车牌号',
    `driver` varchar(55) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '驾驶员',
    `status` tinyint(2) DEFAULT '1' COMMENT '状态 1:正常 2:维修中 3:保养中 4:报废',
    `insurance_date` date DEFAULT NULL COMMENT '保险有效期',
    `check_date` date DEFAULT NULL COMMENT '下次年检日期',
    `travel_distance` decimal(12,2) DEFAULT NULL COMMENT '行驶里程数',
    `maintain_date` date DEFAULT NULL COMMENT '上次保养日期',
    `maintain_distance` decimal(12,2) DEFAULT NULL COMMENT '上次保养里程数',
    `create_time` datetime DEFAULT NULL COMMENT '更新时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='车辆信息表';

-- ----------------------------
-- Records of {{$pk}}admin_car
-- ----------------------------
BEGIN;
INSERT INTO `{{$pk}}admin_car` VALUES (1, 1, 5, '别克君威', '苏A1176R', '白师傅', 1, '2022-11-01', '2026-11-01', 15000.00, '2021-10-05', 13000.00, '2021-11-01 22:42:59', '2021-11-01 22:42:59');
INSERT INTO `{{$pk}}admin_car` VALUES (2, 4, 7, '别克GL8', '苏A12993', '李师傅', 1, '2022-11-01', '2026-11-01', 15660.00, '2021-10-05', 11000.00, '2021-11-02 11:01:09', '2021-11-02 22:35:58');
COMMIT;

-- ----------------------------
-- Table structure for {{$pk}}admin_car_apply
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_car_apply`;
CREATE TABLE `{{$pk}}admin_car_apply` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '申请id',
    `admin_id` int(11) DEFAULT NULL COMMENT '申请人id',
    `admin_name` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '申请人',
    `car_id` int(11) DEFAULT NULL COMMENT '车辆id',
    `car_number` varchar(55) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '车牌号',
    `car_type` varchar(2) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '车辆类型',
    `passengers` int(3) DEFAULT NULL COMMENT '可乘坐人数',
    `start_area` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '出发地',
    `end_area` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '目的地',
    `start` datetime DEFAULT NULL COMMENT '用车开始时间',
    `end` datetime DEFAULT NULL COMMENT '用车结束时间',
    `people_num` int(3) DEFAULT NULL COMMENT '乘车人数',
    `url` varchar(55) COLLATE utf8mb4_bin DEFAULT 'javascript:;' COMMENT '跳转地址',
    `reason` text COLLATE utf8mb4_bin COMMENT '用车事由',
    `status` tinyint(2) DEFAULT '1' COMMENT '状态 1:审核中 2:申请通过 3:申请失败',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`),
    KEY `idx_car_id` (`car_id`,`start`,`end`,`status`) USING BTREE COMMENT '检索空闲'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用车申请';

-- ----------------------------
-- Table structure for {{$pk}}admin_cate
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_cate`;
CREATE TABLE `{{$pk}}admin_cate` (
    `cate_id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(155) DEFAULT NULL COMMENT '分类名称',
    `parent_id` int(11) DEFAULT NULL COMMENT '上级分类',
    `status` tinyint(2) DEFAULT '1' COMMENT '1 启用 2 禁用',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`cate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='文章分类';

-- ----------------------------
-- Records of {{$pk}}admin_cate
-- ----------------------------
BEGIN;
INSERT INTO `{{$pk}}admin_cate` VALUES (1, '行业新闻', 0, 1, '2021-10-20 13:49:13', NULL);
INSERT INTO `{{$pk}}admin_cate` VALUES (2, 'cms新闻', 1, 1, '2021-10-20 13:49:29', NULL);
INSERT INTO `{{$pk}}admin_cate` VALUES (3, '数码', 0, 1, '2021-10-20 14:45:49', NULL);
INSERT INTO `{{$pk}}admin_cate` VALUES (4, '手机', 3, 1, '2021-10-20 14:48:06', NULL);
COMMIT;

-- ----------------------------
-- Table structure for {{$pk}}admin_department
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_department`;
CREATE TABLE `{{$pk}}admin_department` (
    `dept_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门id',
    `name` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '部门名称',
    `parent_id` int(11) DEFAULT NULL COMMENT '上级id',
    `desc` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '部门介绍',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='部门表';

-- ----------------------------
-- Records of {{$pk}}admin_department
-- ----------------------------
BEGIN;
INSERT INTO `{{$pk}}admin_department` VALUES (1, '总公司', 0, '公司总部', '2021-11-01 10:38:17', NULL);
INSERT INTO `{{$pk}}admin_department` VALUES (2, '技术事业部', 1, '主管公司自研产品研发', '2021-11-01 10:38:41', NULL);
INSERT INTO `{{$pk}}admin_department` VALUES (3, '南京分公司', 0, '这是南京分部', '2021-11-01 13:36:59', '2021-11-01 13:36:59');
INSERT INTO `{{$pk}}admin_department` VALUES (4, '产品部门', 3, '专门负责公司产品的调研，研发', '2021-11-01 13:52:48', '2021-11-01 13:52:48');
COMMIT;

-- ----------------------------
-- Table structure for {{$pk}}admin_form
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_form`;
CREATE TABLE `{{$pk}}admin_form` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表单id',
    `title` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '表单名称',
    `table` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '表名',
    `now_version` int(11) DEFAULT '1' COMMENT '当前版本',
    `deploy_version` int(11) DEFAULT '0' COMMENT '部署版本',
    `status` tinyint(2) DEFAULT '1' COMMENT '部署状态 1 未部署 2 已部署',
    `form_json` longtext COLLATE utf8mb4_bin COMMENT '设计元素',
    `conf_json` longtext COLLATE utf8mb4_bin COMMENT '生成配置文件',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '编辑时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='表单设计表';

-- ----------------------------
-- Table structure for {{$pk}}admin_links
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_links`;
CREATE TABLE `{{$pk}}admin_links` (
    `link_id` int(11) NOT NULL AUTO_INCREMENT,
    `site_name` varchar(255) DEFAULT NULL COMMENT '网址名称',
    `web_url` varchar(255) DEFAULT NULL COMMENT '链接地址',
    `sort` int(3) DEFAULT '1' COMMENT '排序',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`link_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='友情链接';

-- ----------------------------
-- Records of {{$pk}}admin_links
-- ----------------------------
BEGIN;
INSERT INTO `{{$pk}}admin_links` VALUES (1, '百度', 'https://www.baidu.com', 10, '2021-10-20 18:37:56', '2021-10-20 20:55:46');
COMMIT;

-- ----------------------------
-- Table structure for {{$pk}}admin_permission
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_permission`;
CREATE TABLE `{{$pk}}admin_permission` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级ID',
    `title` varchar(50) DEFAULT NULL COMMENT '名称',
    `href` varchar(50) NOT NULL COMMENT '地址',
    `icon` varchar(50) DEFAULT NULL COMMENT '图标',
    `sort` tinyint(4) NOT NULL DEFAULT '99' COMMENT '排序',
    `type` tinyint(1) DEFAULT '1' COMMENT '菜单',
    `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
    PRIMARY KEY (`id`),
    KEY `pid` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8 COMMENT='权限表';

-- ----------------------------
-- Records of {{$pk}}admin_permission
-- ----------------------------
BEGIN;
INSERT INTO `{{$pk}}admin_permission` VALUES (1, 0, '企业员工', '', 'layui-icon layui-icon-username', 2, 0, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (2, 1, '组织架构', '/admin.admin/index', '', 1, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (3, 2, '新增员工', '/admin.admin/add', '', 1, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (4, 2, '编辑员工', '/admin.admin/edit', '', 1, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (5, 2, '修改员工', '/admin.admin/status', '', 1, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (6, 2, '删除员工', '/admin.admin/remove', '', 1, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (7, 2, '批量删除员工', '/admin.admin/batchRemove', '', 1, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (8, 2, '员工分配角色', '/admin.admin/role', '', 1, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (9, 2, '员工分配直接权限', '/admin.admin/permission', '', 1, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (10, 2, '员工回收站', '/admin.admin/recycle', '', 1, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (11, 1, '角色管理', '/admin.role/index', '', 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (12, 11, '新增角色', '/admin.role/add', '', 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (13, 11, '编辑角色', '/admin.role/edit', '', 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (14, 11, '删除角色', '/admin.role/remove', '', 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (15, 11, '角色分配权限', '/admin.role/permission', '', 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (16, 11, '角色回收站', '/admin.role/recycle', '', 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (17, 22, '菜单权限', '/admin.permission/index', '', 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (18, 17, '新增菜单', '/admin.permission/add', '', 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (19, 17, '编辑菜单', '/admin.permission/edit', '', 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (20, 17, '修改菜单状态', '/admin.permission/status', '', 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (21, 17, '删除菜单', '/admin.permission/remove', '', 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (22, 0, '系统管理', '', 'layui-icon layui-icon-set', 3, 0, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (23, 22, '后台日志', '/admin.admin/log', '', 2, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (24, 23, '清空管理员日志', '/admin.admin/removeLog', '', 1, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (25, 22, '系统设置', '/config/index', '', 1, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (26, 22, '图片管理', '/admin.photo/index', '', 2, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (27, 26, '新增图片文件夹', '/admin.photo/add', '', 2, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (28, 26, '删除图片文件夹', '/admin.photo/del', '', 2, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (29, 26, '图片列表', '/admin.photo/list', '', 2, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (30, 26, '添加单图', '/admin.photo/addPhoto', '', 2, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (31, 26, '添加多图', '/admin.photo/addPhotos', '', 2, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (32, 26, '删除图片', '/admin.photo/remove', '', 2, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (33, 26, '批量删除图片', '/admin.photo/batchRemove', '', 2, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (35, 0, '内容管理', '', 'layui-icon layui-icon-website', 10, 0, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (37, 35, '栏目管理', '/admin.cate/index', '', 10, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (38, 35, '文章管理', '/admin.article/index', '', 10, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (39, 35, '友链管理', '/admin.links/index', 'layui-iconlayui-icon-face-smile', 10, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (41, 39, '增加友情链接', '/admin.links/add', '', 10, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (42, 39, '编辑友情链接', '/admin.links/edit', '', 10, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (43, 39, '删除友情链接', '/admin.links/del', '', 10, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (44, 38, '新增文章', '/admin.article/add', '', 10, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (45, 38, '编辑文章', '/admin.article/edit', '', 10, 0, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (46, 38, '删除文章', '/admin.article/del', '', 10, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (47, 37, '新增栏目', '/admin.cate/add', '', 10, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (48, 37, '编辑栏目', '/admin.cate/edit', '', 10, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (49, 37, '删除栏目', '/admin.cate/del', '', 10, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (50, 1, '部门管理', '/admin.department/index', 'layui-icon layui-icon-fire', 99, 2, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (51, 50, '新增部门管理', '/admin.department/add', NULL, 99, 2, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (52, 50, '修改部门管理', '/admin.department/edit', NULL, 99, 2, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (53, 50, '删除部门管理', '/admin.department/remove', NULL, 99, 2, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (54, 1, '离职员工', '/admin.employee/index', 'layui-iconlayui-icon-face-smile', 10, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (55, 1, '试用期员工', '/admin.employee/trial', 'layui-iconlayui-icon-face-smile', 10, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (56, 55, '通知转正', '/admin.employee/notice', 'layui-iconlayui-icon-face-smile', 10, 2, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (57, 1, '职位管理', '/admin.profession/index', 'layui-icon layui-icon-fire', 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (58, 57, '新增职位管理', '/admin.profession/add', NULL, 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (59, 57, '修改职位管理', '/admin.profession/edit', NULL, 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (60, 57, '删除职位管理', '/admin.profession/remove', NULL, 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (61, 57, '批量删除职位管理', '/admin.profession/batchRemove', NULL, 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (63, 0, '办公资源', '', 'layui-icon layui-icon-auz', 10, 0, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (64, 63, '车辆管理', '/admin.car/index', 'layui-icon layui-icon-fire', 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (65, 64, '新增车辆管理', '/admin.car/add', NULL, 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (66, 64, '修改车辆管理', '/admin.car/edit', NULL, 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (67, 64, '删除车辆管理', '/admin.car/remove', NULL, 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (68, 64, '批量删除车辆管理', '/admin.car/batchRemove', NULL, 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (69, 63, '车辆预约管理', '/admin.car/subscribe', 'layui-iconlayui-icon-face-smile', 99, 1, 1);
INSERT INTO `{{$pk}}admin_permission` VALUES (70, 69, '申请预约', '/admin.car/doSubscribe', 'layui-iconlayui-icon-face-smile', 10, 2, 1);
COMMIT;

-- ----------------------------
-- Table structure for {{$pk}}admin_photo
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_photo`;
CREATE TABLE `{{$pk}}admin_photo` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `name` varchar(50) NOT NULL COMMENT '文件名称',
    `href` varchar(255) DEFAULT NULL COMMENT '文件路径',
    `path` varchar(30) DEFAULT NULL COMMENT '路径',
    `mime` varchar(50) NOT NULL COMMENT 'mime类型',
    `size` varchar(30) NOT NULL COMMENT '大小',
    `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1本地2阿里云',
    `ext` varchar(10) DEFAULT NULL COMMENT '文件后缀',
    `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图片表';

-- ----------------------------
-- Records of {{$pk}}admin_photo
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for {{$pk}}admin_profession
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_profession`;
CREATE TABLE `{{$pk}}admin_profession` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '职位id',
    `name` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '职位名称',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='职位表';

-- ----------------------------
-- Records of {{$pk}}admin_profession
-- ----------------------------
BEGIN;
INSERT INTO `{{$pk}}admin_profession` VALUES (1, '总经理', '2021-11-01 18:25:28', '2021-11-01 18:25:28');
INSERT INTO `{{$pk}}admin_profession` VALUES (2, '营销组长', '2021-11-01 18:26:41', '2021-11-01 18:26:41');
INSERT INTO `{{$pk}}admin_profession` VALUES (3, '技术总监', '2021-11-01 18:26:48', '2021-11-01 18:26:48');
INSERT INTO `{{$pk}}admin_profession` VALUES (4, '项目专员', '2021-11-01 18:26:55', '2021-11-01 18:27:15');
COMMIT;

-- ----------------------------
-- Table structure for {{$pk}}admin_role
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_role`;
CREATE TABLE `{{$pk}}admin_role` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `name` varchar(30) DEFAULT NULL COMMENT '名称',
    `desc` varchar(100) DEFAULT NULL COMMENT '描述',
    `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
    `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
    `delete_time` timestamp NULL DEFAULT NULL COMMENT '删除时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of {{$pk}}admin_role
-- ----------------------------
BEGIN;
INSERT INTO `{{$pk}}admin_role` VALUES (1, '超级管理员', '拥有所有管理权限', '2020-09-01 11:01:34', '2020-09-01 11:01:34', NULL);
INSERT INTO `{{$pk}}admin_role` VALUES (2, '主管', '部门主管', '2021-10-29 16:45:22', '2021-10-29 16:45:22', NULL);
COMMIT;

-- ----------------------------
-- Table structure for {{$pk}}admin_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_role_permission`;
CREATE TABLE `{{$pk}}admin_role_permission` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `role_id` int(11) DEFAULT NULL COMMENT '角色ID',
    `permission_id` int(11) DEFAULT NULL COMMENT '权限ID',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色-权限中间表';

-- ----------------------------
-- Records of {{$pk}}admin_role_permission
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for {{$pk}}admin_update_log
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin_update_log`;
CREATE TABLE `{{$pk}}admin_update_log` (
    `log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `version` varchar(55) DEFAULT NULL COMMENT '更新版本',
    `update_date` date DEFAULT NULL COMMENT '更新时间',
    `update_content` text COMMENT '更新内容',
    `fix_content` text COMMENT '修复内容',
    `is_delete` tinyint(2) DEFAULT '1' COMMENT '1 正常 2:删除',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of {{$pk}}admin_update_log
-- ----------------------------
BEGIN;
INSERT INTO `{{$pk}}admin_update_log` VALUES (1, '1.0', '2021-10-20', '强化虚拟模型支持\n改进模型事件和数据库事件\n改进动态获取器处理\n优化分页查询\n改进聚合查询\n关联增加withoutField方法\n软删除destroy方法优化', '增加LogRecord事件\n消除Validate类某处类型声明警告\n路由分组增加dispatcher方法，支持设置分组的调度\nRequest类增加all方法支持获取包括File在内的所有参数\n改进环境变量定义支持多env文件读取', 1, '2021-10-20 11:16:00', '2021-10-20 11:27:56');
COMMIT;

DROP TABLE IF EXISTS `{{$pk}}images`;
CREATE TABLE `{{$pk}}images` (
    `img_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图片名称',
    `sha1` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文件标识',
    `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '绝对地址',
    `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '物理地址',
    `folder` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '所在文件夹',
    `type` enum('web','local','Aliyun') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'local' COMMENT '存储引擎',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`img_id`),
    KEY `idx_folder` (`folder`(191)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='图片表';

SET FOREIGN_KEY_CHECKS = 1;