<?php
declare (strict_types = 1);

namespace app\admin\controller\admin;

use app\common\model\AdminAdmin as M;

class Employee extends  \app\admin\controller\Base
{
    protected $middleware = ['AdminCheck','AdminPermission'];

    public function index()
    {
        if (\request()->isAjax()) {
            return $this->getJson(M::getQuitList());
        }

        return $this->fetch();
    }

    public function trial()
    {
        if (\request()->isAjax()) {
            return $this->getJson(M::getTrialList());
        }

        return $this->fetch();
    }

    public function notice()
    {

    }
}