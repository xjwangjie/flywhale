<?php

declare (strict_types = 1);

namespace app\admin\controller\admin;

use app\admin\controller\Base;
use app\common\model\AdminLinks;
use app\common\model\AdminUpdateLog;

class Links extends Base
{
    protected $middleware = ['AdminCheck','AdminPermission'];

    public function index()
    {
        if (request()->isAjax()) {

            $limit = input('param.limit');
            $where = [];

            $linkModel = new AdminLinks();
            $list = $linkModel->getLinksList($where, $limit);

            return json(pageReturn($list));
        }

        return $this->fetch();
    }

    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $param['create_time'] = date('Y-m-d H:i:s');
            AdminLinks::insert($param);

            return jsonReturn(0, '新增成功');
        }

        return $this->fetch();
    }

    public function edit()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $param['update_time'] = date('Y-m-d H:i:s');
            AdminLinks::where('link_id', $param['link_id'])->update($param);

            return jsonReturn(0, '更新成功');
        }

        $id = input('param.id');

        $info = AdminLinks::where('link_id', $id)->find();
        return $this->fetch('', [
            'info' => $info
        ]);
    }

    public function del()
    {
        $id = input('param.id');

        AdminLinks::where('link_id', $id)->delete();

        return jsonReturn(0, '删除成功');
    }
}