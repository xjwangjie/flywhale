<?php

return [

    // socket.io 端口配置
    'ws_port' => 6020,

    // http端口配置
    'http_port' => 21102,

    // 是否开启ssl
    'open_ssl' => env('SSL.IS_OPEN', false),

    // ssl配置文件
    'context' => [
        'ssl' => [
            'local_cert'  => env('SSL.LOCAL_CERT', ''),
            'local_pk'    => env('SSL.LOCAL_PK', ''),
            'verify_peer' => false,
        ]
    ]
];